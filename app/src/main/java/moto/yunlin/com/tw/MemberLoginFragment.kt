package moto.yunlin.com.tw

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_login.*
import moto.yunlin.com.tw.Api.LoginApi
import moto.yunlin.com.tw.Model.UserInfoModel
import okhttp3.Call
import okhttp3.Response
import java.io.IOException
import android.content.Context





class MemberLoginFragment : Fragment() {

    private var sp: SP? = null
    private var callBackValue: CallBackValue? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callBackValue = activity as CallBackValue?

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        sp = SP(activity)

        memberLogin.setOnClickListener {
            val acc = memberLoginAcc.text.toString()
            val pwd = memberLoginPwd.text.toString()
            if (acc.isNotEmpty() && pwd.isNotEmpty()) {
                progressBar3.visibility = View.VISIBLE
                LoginApi().login(acc,pwd,Callback())
            }
        }

        memberLoginSigUp.setOnClickListener {
            val intent = Intent(activity,SignUpActivity::class.java)
            intent.putExtra(SignUpActivity.PROPOSE,"signUp")
            activity?.startActivity(intent)
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            activity?.runOnUiThread {
                progressBar3.visibility = View.INVISIBLE
                e.printStackTrace()
                androidx.appcompat.app.AlertDialog.Builder(context!!)
                    .setTitle("訊息提示")
                    .setMessage("網路或系統有誤，請稍後再試！")
                    .setPositiveButton("確定") { d, w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            val listType = object :TypeToken<UserInfoModel>(){}.type
            val userInfoModel: UserInfoModel = Gson().fromJson(mResponse,listType)

            if (userInfoModel.code == "SUCCESS") {
                sp?.setid(userInfoModel.payload?.memberId)
                sp?.setpw(memberLoginPwd.text.toString())
                sp?.setemail(userInfoModel.payload?.email)
                sp?.setname(userInfoModel.payload?.name)
                sp?.setphoneNumber(userInfoModel.payload?.phoneNumber)
                sp?.setmobileNumber(userInfoModel.payload?.mobileNumber)
                sp?.setaddress(userInfoModel.payload?.address)
                activity?.runOnUiThread {
                    progressBar3.visibility = View.INVISIBLE
                    callBackValue?.SendValue(true)
                    val fm = activity?.supportFragmentManager
                    val transaction = fm?.beginTransaction()
                    transaction?.replace(R.id.fragment_container, MemberInfoFragment())?.commit()
                }
            } else {
                activity?.runOnUiThread {
                    progressBar3.visibility = View.INVISIBLE
                    Toast.makeText(activity,userInfoModel.errMsg,Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    interface CallBackValue {
        fun SendValue(isLogin: Boolean)
    }
}