package moto.yunlin.com.tw.yunlin.Api

import moto.yunlin.com.tw.GlobalVar.feedbackInfoApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class FeedbackApi {

    val feedbackClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun feedback (carNo:String,ownerName:String,ownerMobileNumber:String,ownerPhoneNumber:String,ownerAddress:String,informerDeviceId:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("carNo", carNo)
            .add("ownerName", ownerName)
            .add("ownerMobileNumber", ownerMobileNumber)
            .add("ownerPhoneNumber", ownerPhoneNumber)
            .add("ownerAddress", ownerAddress)
            .add("informerDeviceId", informerDeviceId)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(feedbackInfoApiUrl )
            .build()

        val call: Call = feedbackClient.newCall(request)

        call.enqueue(callback)

    }

}