package moto.yunlin.com.tw.Api

import moto.yunlin.com.tw.GlobalVar.queryCarNoApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class QueryCarNoApi {

    val queryCarNoClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun queryCarNo (carNo:String,memberId:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("carNo", carNo)
            .add("memberId", memberId)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(queryCarNoApiUrl)
            .build()

        val call: Call = queryCarNoClient.newCall(request)

        call.enqueue(callback)

    }
}