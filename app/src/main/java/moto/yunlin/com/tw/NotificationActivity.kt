package moto.yunlin.com.tw

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_notification.*
import moto.yunlin.com.tw.Adapter.NotificationAdapter

class NotificationActivity : AppCompatActivity() {

    private var nAdapter: NotificationAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        initView()

    }

    private fun initView() {

        notiRv.layoutManager = LinearLayoutManager(this)
        nAdapter = NotificationAdapter(this)
        notiRv.adapter = nAdapter

        iv_back2.setOnClickListener {
            finish()
        }
    }
}