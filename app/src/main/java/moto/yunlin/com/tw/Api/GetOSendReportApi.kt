package moto.yunlin.com.tw.Api

import moto.yunlin.com.tw.GlobalVar.reportApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class GetOSendReportApi {

    val getOSendReportClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun getOrSendReport (reportId:String, rpLatitude:String, rpLongitude:String, rpPlace:String, callback: Callback) {

        val body = FormBody.Builder()
            .add("reportId", reportId)
            .add("rpLatitude", rpLatitude)
            .add("rpLongitude", rpLongitude)
            .add("rpPlace", rpPlace)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(reportApiUrl)
            .build()

        val call: Call = getOSendReportClient.newCall(request)

        call.enqueue(callback)

    }
}