package moto.yunlin.com.tw

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_inspection_result.*
import moto.yunlin.com.tw.Model.QueryCarNoModel
import org.json.JSONException

class QueryCarNoResultActivity: AppCompatActivity() {

    companion object{
        const val CARINFO = "carInfo"
        const val FROM = "from"
    }

    private var check = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspection_result)

        initView()

    }

    private fun initView() {

        val listType = object: TypeToken<QueryCarNoModel>() {}.type
        val queryCarNoModel: QueryCarNoModel = Gson().fromJson(intent.getStringExtra(CARINFO),listType)

        if (intent.getStringExtra(FROM) == "inspection") {
            tv_send.visibility = View.GONE
        }

        tv1.text = queryCarNoModel.payload?.carNo
        tv2.text = queryCarNoModel.payload?.checkStatusMsg
        tv3.text = queryCarNoModel.payload?.carBrand
        tv4.text = queryCarNoModel.payload?.carCC
        tv6.text = queryCarNoModel.payload?.carOutDate
        tv7.text = queryCarNoModel.payload?.LIC
        tv8.text = queryCarNoModel.payload?.LICPeriod
        tv9.text = queryCarNoModel.payload?.lastCheckDate
        tv10.text = queryCarNoModel.payload?.HC
        tv11.text = queryCarNoModel.payload?.CO
        tv12.text = queryCarNoModel.payload?.CO2

        iv_back.setOnClickListener {
            finish()
        }

        when (queryCarNoModel.payload?.checkStatus) {
            "1", "-4", "6" -> {
                Glide.with(this).load(R.drawable.ico_pass)
                    .centerCrop()
                    .into(iv_status)
                tv2.setTextColor(Color.BLUE)
            }
            "2", "3", "7" -> {
                Glide.with(this).load(R.drawable.ico_notpass)
                    .centerCrop()
                    .into(iv_status)
                tv2.setTextColor(Color.RED)
            }
            "4" -> {
                Glide.with(this).load(R.drawable.ico_uncheck)
                    .centerCrop()
                    .into(iv_status)
                tv2.setTextColor(Color.GREEN)
            }
        }

        if (queryCarNoModel.payload?.valid?.result == "overdue") {
            if (intent.getStringExtra("from") == "feedback") {
                check_rl.visibility = View.VISIBLE
            }

            textView14.setOnClickListener {
                AlertDialog.Builder(this)
                    .setTitle("會員服務條款")
                    .setMessage("本人同意雲林縣環境保護局，於雲林縣機車定檢服務及未定檢回報系統整合APP系統使用時，利用訊息通知提醒機車排氣定期檢驗及獎勵資訊發佈之特定目的範圍內，蒐集本人個人資料，亦得提供其所蒐集本人個人資料提供給予電信業者利用及電腦處理通知作業（本資料僅供APP管理使用）。")
                    .setPositiveButton(
                        "確認"
                    ) { dialog, which ->
                        dialog.cancel()
                    }.show()
            }

            checkBox.setOnClickListener {
                check = if (!check) {
                    checkBox.setImageResource(R.drawable.input_radio_on)
                    true
                } else {
                    checkBox.setImageResource(R.drawable.input_radio_off)
                    false
                }
            }

            tv_send.setTextColor(Color.parseColor("#FFFFFF"))
            tv_send.text = "我要檢舉"
            tv_send.setOnClickListener {
                var msg = ""
                if (!check) {
                    Toast.makeText(this,"請確認服務條款",Toast.LENGTH_LONG).show()
                } else {
                    try {
                        when (queryCarNoModel.payload.reportStatus) {
                            "processing" -> msg = "此車號你的檢舉受理中, 可至案件回饋查詢受理進度"
                            "duplicate" -> msg = "本車不符合檢舉條件"
                            "notYL" -> msg = "非雲林籍車輛,無法檢舉"
                        }
                        when {
                            queryCarNoModel.payload.reportId?.isNotEmpty() == true -> {
//                                AlertDialog.Builder(this)
//                                    .setTitle("訊息提示")
//                                    .setMessage("此功能維修中，將盡快上線！")
//                                    .setPositiveButton("確定") {d,w ->
//                                        d.cancel()
//                                    }
//                                    .show()
                                val i = Intent(this, MotoReportActivity::class.java)
                                i.putExtra(MotoReportActivity.REPORTID,queryCarNoModel.payload.reportId)
                                i.putExtra(MotoReportActivity.CARNO,queryCarNoModel.payload.carNo)
                                startActivity(i)
                                finish()
                            }
                            msg.isNotEmpty() -> AlertDialog.Builder(this)
                                .setTitle("訊息提示")
                                .setMessage(msg)
                                .setPositiveButton(
                                    "確認"
                                ) { dialog, which -> dialog.dismiss() }.show()
                            else -> AlertDialog.Builder(this)
                                .setTitle("訊息提示")
                                .setMessage("很抱歉，無法取得檢舉單號，請聯絡管理員")
                                .setPositiveButton(
                                    "確認"
                                ) { dialog, which -> dialog.dismiss() }.show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        } else {
            tv_send.text = "無法檢舉"
        }
    }
}