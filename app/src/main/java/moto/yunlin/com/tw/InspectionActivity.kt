package moto.yunlin.com.tw

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_inspection.*
import moto.yunlin.com.tw.Api.QueryCarNoApi
import moto.yunlin.com.tw.Model.QueryCarNoModel
import okhttp3.Call
import okhttp3.Response
import java.io.IOException

class InspectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspection)

        initView()

    }

    private fun initView() {

        var sp = SP(this)
        var id = ""
        if (sp.getid().isNotEmpty()) {
            id = sp.getid()
        }

        imageView3.setOnClickListener {
            finish()
        }

        inspectionDone.setOnClickListener {
            var carNo = inspectionNo.text.toString()
            if (carNo.isNotEmpty()) {
                carNo = carNo.replace("-","")
                progressBar.visibility = View.VISIBLE
                QueryCarNoApi().queryCarNo(carNo,id,Callback())
            }
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar.visibility = View.INVISIBLE
                e.printStackTrace()
                AlertDialog.Builder(this@InspectionActivity)
                    .setTitle("訊息提示")
                    .setMessage("網路或系統錯誤，請稍後再試！")
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            Log.d("queryCarNoApi-response",mResponse)
            val listType = object : TypeToken<QueryCarNoModel>() {}.type
            val queryCarNoModel: QueryCarNoModel = Gson().fromJson(mResponse, listType)

            if (queryCarNoModel.code.equals("SUCCESS")) {
                if(queryCarNoModel.payload?.checkStatus.equals("-3")) {
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        AlertDialog.Builder(this@InspectionActivity)
                            .setTitle("訊息提示")
                            .setMessage("無此車號")
                            .setPositiveButton("確定") {d,w ->
                                d.cancel()
                            }
                            .show()
                    }
                } else {
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        val intent = Intent()
                        intent.putExtra(QueryCarNoResultActivity.CARINFO,mResponse)
                        intent.putExtra(QueryCarNoResultActivity.FROM,"inspection")
                        intent.setClass(this@InspectionActivity,QueryCarNoResultActivity::class.java)
                        startActivity(intent)
                    }
                }
            } else {
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@InspectionActivity)
                        .setTitle("訊息提示")
                        .setMessage("系統或資料有誤，請稍後再試！")
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}