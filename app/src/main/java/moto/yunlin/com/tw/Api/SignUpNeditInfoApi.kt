package moto.yunlin.com.tw.Api

import moto.yunlin.com.tw.GlobalVar.signUpNeditInfoApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class SignUpNeditInfoApi {

    val signUpNeditInfoClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun signUpNeditInfo (deviceId:String,memberId:String,name:String,email:String,pwd:String,mobileNumber:String,phoneNumber:String,ads:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("deviceId", deviceId)
            .add("memberId", memberId)
            .add("deviceOSType", "android")
            .add("name", name)
            .add("email", email)
            .add("password", pwd)
            .add("mobileNumber", mobileNumber)
            .add("phoneNumber", phoneNumber)
            .add("address", ads)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(signUpNeditInfoApiUrl)
            .build()

        val call: Call = signUpNeditInfoClient.newCall(request)

        call.enqueue(callback)

    }
}