package moto.yunlin.com.tw

import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_member_main.*
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class MemberActivity : AppCompatActivity(),MemberLoginFragment.CallBackValue{

    private var flagList = mutableListOf(true,false,false)
    private var sp: SP? = null
    private var isLogin = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_main)

        initView()

    }

    private fun initView() {

        sp = SP(this)
        isLogin = sp?.getname()?.isNotEmpty() == true

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()
        var fragment = when (isLogin) {
            true -> MemberInfoFragment()
            false -> MemberLoginFragment()
        }
        fragmentTransaction?.add(R.id.fragment_container,fragment)?.commit()

        imageView10.setOnClickListener {
            finish()
        }

        memberMain.setOnClickListener {
            if (!flagList[0]) {
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager?.beginTransaction()
                var fragment = when (isLogin) {
                    true -> MemberInfoFragment()
                    false -> MemberLoginFragment()
                }
                fragmentTransaction?.replace(R.id.fragment_container,fragment)?.commit()
                memberMain.setTextColor(Color.parseColor("#0000FF"))
                memberFeedback.setTextColor(Color.parseColor("#000000"))
                memberCaseFeedback.setTextColor(Color.parseColor("#000000"))
                memberMainTitle.text = "會員資料"
                flagList[0] = true
                flagList[1] = false
                flagList[2] = false

            }
        }

        memberFeedback.setOnClickListener {
            if (!flagList[1]) {
                val transaction = fragmentManager?.beginTransaction()
                transaction?.replace(R.id.fragment_container, MemberFeedbackFragment())?.commit()
                memberFeedback.setTextColor(Color.parseColor("#0000FF"))
                memberMain.setTextColor(Color.parseColor("#000000"))
                memberCaseFeedback.setTextColor(Color.parseColor("#000000"))
                memberMainTitle.text = "我要檢舉"
                flagList[1] = true
                flagList[0] = false
                flagList[2] = false
            }
        }

        memberCaseFeedback.setOnClickListener {
            if (isLogin) {
                if (!flagList[2]) {
                    val transaction = fragmentManager?.beginTransaction()
                    transaction?.replace(R.id.fragment_container, MemberCaseFeedbackFragment())
                        ?.commit()
                    memberCaseFeedback.setTextColor(Color.parseColor("#0000FF"))
                    memberMain.setTextColor(Color.parseColor("#000000"))
                    memberFeedback.setTextColor(Color.parseColor("#000000"))
                    memberMainTitle.text = "案件回饋"
                    flagList[2] = true
                    flagList[1] = false
                    flagList[0] = false
                }
            } else {
                Toast.makeText(this,"請先登入會員！",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun SendValue(mIsLogin: Boolean) {
        isLogin = mIsLogin
    }
}