package moto.yunlin.com.tw

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_inspection_station.*
import moto.yunlin.com.tw.Adapter.InspectionStationAdapter
import moto.yunlin.com.tw.Api.GetStationApi
import moto.yunlin.com.tw.GlobalVar.areaList
import moto.yunlin.com.tw.GlobalVar.stationList
import moto.yunlin.com.tw.Model.InspectionStationModel
import moto.yunlin.com.tw.Model.StationDetailModel
import moto.yunlin.com.tw.Model.StationInfoModel
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import java.io.Serializable
import java.util.concurrent.TimeoutException

class InspectionStationActivity: AppCompatActivity() {

    private var nAdapter: InspectionStationAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspection_station)

        initView()

    }

    private fun initView() {

        GetStationApi().getStation(Callback())

        nAdapter = InspectionStationAdapter(this) {
            val intent = Intent(this,StationDetailActivity::class.java)
            intent.putExtra(StationDetailActivity.AREA,it)
            intent.putExtra(StationDetailActivity.STATIONINFO,stationList as Serializable)
            startActivity(intent)
        }

        inspectionStationRv.layoutManager = LinearLayoutManager(this)
        inspectionStationRv.adapter = nAdapter

        imageView9.setOnClickListener {
            finish()
        }

        inspectionStationNearBy.setOnClickListener {
            if (stationList.isNotEmpty()) {
                val intent = Intent(this, MapsActivity::class.java)
                intent.putExtra(MapsActivity.STATIONINFO, stationList as Serializable)
                intent.putExtra(MapsActivity.TITLE, "附近定檢站")
                intent.putExtra(MapsActivity.FROM, "nearBy")
                startActivity(intent)
            } else {
                Toast.makeText(this,"資料載入未完成！", Toast.LENGTH_LONG).show()
            }
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar8.visibility = View.INVISIBLE
                e.printStackTrace()
                AlertDialog.Builder(this@InspectionStationActivity)
                    .setTitle("訊息提示")
                    .setMessage("系統或網路出現問題，請稍後再試！")
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            val listType = object : TypeToken<InspectionStationModel>() {}.type
            val inspectionStationModel: InspectionStationModel =
                Gson().fromJson(mResponse, listType)

            if (inspectionStationModel.success) {
                val stationDetailModel = inspectionStationModel.result.records
                for (i in stationDetailModel.indices) {
                    val sno = stationDetailModel.getOrNull(i)?.Sno
                    val area = stationDetailModel.getOrNull(i)?.Address?.substring(3, 6) ?: ""
                    if (sno?.contains("P") == true) {
                        if(stationDetailModel.getOrNull(i)?.Latitude?.isEmpty() == true) {
                            var geo = Geocoder(this@InspectionStationActivity)
                            var latlng = geo.getFromLocationName(stationDetailModel.getOrNull(i)?.Address,1)
                            if (latlng.size != 0 && latlng != null) {
                                stationDetailModel.getOrNull(i)?.Latitude = latlng[0].latitude.toString()
                                stationDetailModel.getOrNull(i)?.Longitude = latlng[0].longitude.toString()
                            }
                        }
                        stationList.add(stationDetailModel.getOrNull(i)!!)
                        if (!areaList.contains(area)) {
                            areaList.add(area)
                        }
                    }
                }
                runOnUiThread {
                    progressBar8.visibility = View.INVISIBLE
                    nAdapter?.updateData(areaList)
                }
            } else {
                runOnUiThread {
                    progressBar8.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@InspectionStationActivity)
                        .setTitle("訊息提示")
                        .setMessage("資料擷取失敗，請稍後再試！")
                        .setPositiveButton("確定") { d, w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}