package moto.yunlin.com.tw.Model

data class CarInfoModel(
    val CO: String? = null,
    val CO2: String? = null,
    val HC: String? = null,
    val LIC: String? = null,
    val LICPeriod: String? = null,
    val carBrand: String? = null,
    val carCC: String? = null,
    val carNo: String? = null,
    val carOutDate: String? = null,
    val checkResult: String? = null,
    val checkStatus: String? = null,
    val checkStatusMsg: String? = null,
    val city: String? = null,
    val lastCheckDate: String? = null,
    val path: String? = null,
    val reportStatus: String? = null,
    val reportId: String? = null,
    val siteNo: String? = null,
    val tag: String? = null,
    val valid: CarValidInfoModel? = null
)