package moto.yunlin.com.tw.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import moto.yunlin.com.tw.R
import moto.yunlin.com.tw.Model.StationDetailModel

class StationDetailAdapter(var context: Context,var click:(Int)->Unit) : RecyclerView.Adapter<StationDetailAdapter.StationDetailViewHolder>() {

    var data : MutableList<StationDetailModel>? = null

    fun updateData(
        data: MutableList<StationDetailModel>?) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationDetailViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_station_detail, parent, false)
        return StationDetailViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data?.size?: 0
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: StationDetailViewHolder, position: Int) {
        Log.d("areSize",data?.size.toString())
        Log.d("area${position+1}","lat:${data!![position].Latitude},lng:${data!![position].Longitude}")
        holder.itemStationName.text = data?.getOrNull(position)?.SName
        holder.itemStationAds.text = data?.getOrNull(position)?.Address
        holder.itemStationPhone.text = data?.getOrNull(position)?.Tel
        holder.itemView.setOnClickListener {
            click(position)
        }
    }

    inner class StationDetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemStationName: TextView = itemView.findViewById(R.id.itemStationName)
        var itemStationAds: TextView = itemView.findViewById(R.id.itemStationAds)
        var itemStationPhone: TextView = itemView.findViewById(R.id.itemStationPhone)

    }
}