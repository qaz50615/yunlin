package moto.yunlin.com.tw

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activitiy_signup.*
import moto.yunlin.com.tw.Api.SignUpNeditInfoApi
import moto.yunlin.com.tw.GlobalVar.fcmToken
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException

class SignUpActivity : AppCompatActivity() {

    companion object{
        const val PROPOSE = "propose"
    }

    private var sp: SP? = null
    private var propose = ""
    private var check = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activitiy_signup)

        initView()

    }

    private fun initView() {

        sp = SP(this)
        propose = intent.getStringExtra(PROPOSE)?:""

        checkBox.setOnClickListener {
            check = if (!check) {
                checkBox.setImageResource(R.drawable.input_radio_on)
                true
            } else {
                checkBox.setImageResource(R.drawable.input_radio_off)
                false
            }
        }

        iv_back.setOnClickListener {
            finish()
        }

        signUpRule.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("『會員服務條款』")
                .setMessage("本人同意雲林縣環境保護局，於雲林縣機車定檢服務及未定檢回報系統整合APP系統使用時，利用訊息通知提醒機車排氣定期檢驗及獎勵資訊發佈之特定目的範圍內，蒐集本人個人資料，亦得提供其所蒐集本人個人資料提供給予電信業者利用及電腦處理通知作業（本資料僅供APP管理使用）。")
                .setPositiveButton("確定") {d,w ->
                    d.cancel()
                }
                .show()
        }

        tv_Rig.setOnClickListener {
            if (check) {
                if (et_pw.text.toString() == et_cpw.text.toString()) {
                    val acc = et_id.text.toString()
                    val pwd = et_pw.text.toString()
                    val name = et_name.text.toString()
                    val email = et_mail.text.toString()
                    val mobilePhone = et_phone.text.toString()
                    val phone = et_phone2.text.toString()
                    val ads = et_address.text.toString()
                    progressBar5.visibility = View.VISIBLE
                    SignUpNeditInfoApi().signUpNeditInfo(fcmToken,acc,name,email,pwd,mobilePhone,phone,ads,Callback())
                } else {
                    Toast.makeText(this,"密碼重複不符！",Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this,"請確認會員服務條款！",Toast.LENGTH_SHORT).show()
            }
        }

        if (propose != "signUp") {
            tv_title.text = "修改會員資料"
            tv_Rig.text = "確認"
            textView7.visibility = View.INVISIBLE
            signUpRule.visibility = View.INVISIBLE
            et_id.setText(sp?.getid())
            et_mail.setText(sp?.getemail())
            et_name.setText(sp?.getname())
            et_pw.setText(sp?.getpw())
            et_cpw.setText(sp?.getpw())
            et_phone.setText(sp?.getphoneNumber())
            et_phone2.setText(sp?.getmobileNumber())
            et_address.setText(sp?.getaddress())
        }

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar5.visibility = View.INVISIBLE
                AlertDialog.Builder(this@SignUpActivity)
                    .setTitle("訊息提示")
                    .setMessage("系統或網路錯誤，請稍後再試！")
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            val jb = JSONObject(mResponse)
            if (jb.getString("code") == "SUCCESS") {
                runOnUiThread {
                    Toast.makeText(this@SignUpActivity,"註冊成功！將盡快為您審核！",Toast.LENGTH_LONG).show()
                    finish()
                }
            } else {
                runOnUiThread {
                    progressBar5.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@SignUpActivity)
                        .setTitle("訊息提示")
                        .setMessage("系統或資料錯誤，請稍後再試！")
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}