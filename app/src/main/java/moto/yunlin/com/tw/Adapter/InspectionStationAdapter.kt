package moto.yunlin.com.tw.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import moto.yunlin.com.tw.R
import moto.yunlin.com.tw.Model.StationDetailModel
import moto.yunlin.com.tw.Model.StationInfoModel

class InspectionStationAdapter(var context: Context,var click:(String)->Unit) : RecyclerView.Adapter<InspectionStationAdapter.InspectionStationViewHolder>() {

    var data : MutableList<String>? = null

    fun updateData(
        data: MutableList<String>?) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InspectionStationViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_inspection_station, parent, false)
        return InspectionStationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data?.size?: 0
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(inspectionStationViewHolder: InspectionStationViewHolder, position: Int) {
        inspectionStationViewHolder.area.text = data?.getOrNull(position)
        inspectionStationViewHolder.itemView.setOnClickListener {
            click(data?.getOrNull(position)!!)
        }
    }

    inner class InspectionStationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var area: TextView = itemView.findViewById(R.id.textView14)
    }

}