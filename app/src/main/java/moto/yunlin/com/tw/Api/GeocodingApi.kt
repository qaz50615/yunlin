package moto.yunlin.com.tw.Api

import okhttp3.*
import java.util.concurrent.TimeUnit

class GeocodingApi {

    val geocodeClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS).build()

    fun geocode (lat:Double,lng:Double,callback: Callback) {

        var url = "https://maps.googleapis.com/maps/api/geocode/json?"
        url += "latlng=${lat},${lng}&"
        url += "key=AIzaSyC7cNds-WAcwx88GIiaXSv-7nrsNluZE_E&language=zh-tw"

        val request = Request.Builder()
            .url(url)
            .get()
            .build()

        val call: Call = geocodeClient.newCall(request)

        call.enqueue(callback)
    }
}