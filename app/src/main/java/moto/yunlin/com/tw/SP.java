package moto.yunlin.com.tw;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ttg on 2016/11/9.
 */

public class SP {
    SharedPreferences sp;
    Context c;

    public SP(Context context) {
        this.c = context;
        sp = c.getSharedPreferences("global", 0);

    }

    public void setid(String id) {
        sp.edit().putString("id", id).commit();
    }

    public String getid() {
        return sp.getString("id", "");
    }

    public void setpw(String pw) {
        sp.edit().putString("pw", pw).commit();
    }

    public String getpw() {
        return sp.getString("pw", "");
    }

    public void setdeviceId(String deviceId) {
        sp.edit().putString("deviceId", deviceId).commit();
    }

    public String getdeviceId() {
        return sp.getString("deviceId", "");
    }

    public void setname(String name) {
        sp.edit().putString("name", name).commit();
    }

    public String getname() {
        return sp.getString("name", "");
    }


    public void setemail(String email) {
        sp.edit().putString("email", email).commit();
    }

    public String getemail() {
        return sp.getString("email", "");
    }


    public void setphoneNumber(String phoneNumber) {
        sp.edit().putString("phoneNumber", phoneNumber).commit();
    }

    public String getphoneNumber() {
        return sp.getString("phoneNumber", "");
    }

    public void setmobileNumber(String mobileNumber) {
        sp.edit().putString("mobileNumber", mobileNumber).commit();
    }

    public String getmobileNumber() {
        return sp.getString("mobileNumber", "");
    }


    public void setaddress(String address) {
        sp.edit().putString("address", address).commit();
    }

    public String getaddress() {
        return sp.getString("address", "");
    }


    public void setmemberStatus(String memberStatus) {
        sp.edit().putString("memberStatus", memberStatus).commit();
    }

    public String getmemberStatus() {
        return sp.getString("memberStatus", "");
    }


    public void setbarcode(String barcode) {
        sp.edit().putString("barcode", barcode).commit();
    }

    public String getbarcode() {
        return sp.getString("barcode", "");
    }

    public void clear() {
        sp.edit().clear().commit();
    }

}
