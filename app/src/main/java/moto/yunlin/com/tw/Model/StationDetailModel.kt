package moto.yunlin.com.tw.Model

import java.io.Serializable

class StationDetailModel: Serializable {
    var  Address: String? = null
    var  Latitude: String? = null
    var  Longitude: String? = null
    var  Note: String? = null
    var  SName: String? = null
    var  Sno: String? = null
    var  Tel: String? = null
}