package moto.yunlin.com.tw.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import moto.yunlin.com.tw.R

class NotificationAdapter(var context: Context) : RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {

    var data : MutableList<String>? = null

    fun updateData(
        data: MutableList<String>?) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_notification, parent, false)
        return NotificationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data?.size?: 0
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.text.text = data?.getOrNull(0)
        holder.text2.text = data?.getOrNull(1)
    }

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text: TextView = itemView.findViewById(R.id.textView18)
        var text2: TextView = itemView.findViewById(R.id.textView12)
    }
}