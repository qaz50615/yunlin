package moto.yunlin.com.tw.Model

data class StationInfoModel(
    val id: String? = null,
    val name: String? = null,
    val address: String? = null,
    val brand: String? = null,
    val lat: Double? = null,
    val lon: Double? = null,
    val tel: String? = null
)