package moto.yunlin.com.tw

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_moto_qrcode.*
import kotlinx.android.synthetic.main.include_qrcode_view.*

class MotoQrcodeActivity : AppCompatActivity(){

    private var sp : SP? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_moto_qrcode)

        initView()

    }

    private fun initView() {

        sp = SP(this)
        val qrcode = sp?.getbarcode()?:""
        if (qrcode.isNotEmpty()) {
            motoQrcodeInclude.visibility = View.VISIBLE
            motoQrcodeDone.visibility = View.INVISIBLE
            madeBarcode(qrcode,inspectionQrcodeImage,inspectionQrcodeContent)
        }

        imageView3.setOnClickListener {
            finish()
        }

        motoQrcodeDone.setOnClickListener {
            if (sp?.getbarcode()?.isEmpty() == true ){
                if (motoQrcodeNo.text.isNotEmpty()) {
                    sp?.setbarcode(motoQrcodeNo.text.toString())
                    motoQrcodeInclude.visibility = View.VISIBLE
                    motoQrcodeDone.visibility = View.INVISIBLE
                    madeBarcode(motoQrcodeNo.text.toString(), inspectionQrcodeImage, inspectionQrcodeContent)
                } else {
                    Toast.makeText(this,"車牌號碼不得空白！",Toast.LENGTH_LONG).show()
                }
            }
        }

        motoQrcodeRebuild.setOnClickListener {
            sp?.setbarcode("")
            motoQrcodeInclude.visibility = View.INVISIBLE
            motoQrcodeLayout.visibility = View.VISIBLE
            motoQrcodeDone.visibility = View.VISIBLE
        }

    }

    private fun madeBarcode(barcode:String, view: ImageView, textView: TextView) {
        var mBarcode = barcode

        if (mBarcode.contains("-")) {
            mBarcode = mBarcode.replace("-","")
        }

        mBarcode = if (mBarcode.length != 6) {
            mBarcode.substring(0, 3) + "-" + mBarcode.substring(3, mBarcode.length)
        } else {
            mBarcode.substring(0, 3) + "-" + mBarcode.substring(3, mBarcode.length)
        }

        val encoder = BarcodeEncoder()
        try {
            val bit = encoder.encodeBitmap(
                mBarcode, BarcodeFormat.CODE_128,500,300
            )
            view.setImageBitmap(bit)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        textView.text = mBarcode
    }
}