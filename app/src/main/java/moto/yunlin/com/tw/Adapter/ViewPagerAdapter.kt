package moto.yunlin.com.tw.Adapter

import android.media.Image
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import java.nio.file.Files.size
import androidx.viewpager.widget.PagerAdapter

class ViewPagerAdapter(data:ArrayList<ImageView>) : PagerAdapter() {

    private var list = data

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return Integer.MAX_VALUE // 要無限輪播
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val position1 = position % list.size
        val imageView = list[position1]
        container.addView(imageView)
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}