package moto.yunlin.com.tw

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_feedbackinfo.*
import moto.yunlin.com.tw.GlobalVar.fcmToken
import moto.yunlin.com.tw.yunlin.Api.FeedbackApi
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class FeedbackActivity : AppCompatActivity() {

    private var check = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedbackinfo)

        initView()

    }

    private fun initView() {

        imageView9.setOnClickListener {
            finish()
        }

        checkBox.setOnClickListener {
            check = if (!check) {
                checkBox.setImageResource(R.drawable.input_radio_on)
                true
            } else {
                checkBox.setImageResource(R.drawable.input_radio_off)
                false
            }
        }

        feedbackInfoRule.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("『會員服務條款』")
                .setMessage("本人同意雲林縣環境保護局，於雲林縣機車定檢服務及未定檢回報系統整合APP系統使用時，利用訊息通知提醒機車排氣定期檢驗及獎勵資訊發佈之特定目的範圍內，蒐集本人個人資料，亦得提供其所蒐集本人個人資料提供給予電信業者利用及電腦處理通知作業（本資料僅供APP管理使用）。")
                .setPositiveButton("確定") {d,w ->
                    d.cancel()
                }
                .show()
        }

        feedbackInfoDone.setOnClickListener {
            var isEmpty = false
            var list = listOf<String>(feedbackInfoMotoNo.text.toString(),feedbackInfoName.text.toString(),feedbackInfoMB.text.toString(),feedbackInfoPhone.text.toString(),feedbackInfoAds.text.toString())
            for (i in list.indices) {
                if (list[i].isEmpty()) {
                    isEmpty = true
                }
            }
            if (!isEmpty) {
                progressBar4.visibility = View.VISIBLE
                FeedbackApi().feedback(list[0],list[1],list[2],list[3],list[4],fcmToken,Callback())
            } else {
                Toast.makeText(this,"欄位皆不得空白！",Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: okhttp3.Call, e: IOException) {
            runOnUiThread {
                progressBar4.visibility = View.INVISIBLE
                e.printStackTrace()
                AlertDialog.Builder(this@FeedbackActivity)
                    .setTitle("訊息提示")
                    .setMessage("網路或系統錯誤，請稍後再試！")
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: okhttp3.Call, response: Response) {
            try {
                val mResponse = response.body()?.string()
                val jsonObject = JSONObject(mResponse)

                if (jsonObject.getString("code") == "SUCCESS") {
                    runOnUiThread {
                        Toast.makeText(this@FeedbackActivity,"發送成功！",Toast.LENGTH_SHORT).show()
                        finish()
                    }
                } else {
                    runOnUiThread {
                        progressBar4.visibility = View.INVISIBLE
                        AlertDialog.Builder(this@FeedbackActivity)
                            .setTitle("訊息提示")
                            .setMessage("系統或資料錯誤，請稍後再試！")
                            .setPositiveButton("確定") { d, w ->
                                d.cancel()
                            }
                            .show()
                    }
                }
            }catch (e:JSONException) {
                e.printStackTrace()
            }
        }
    }
}