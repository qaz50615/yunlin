package moto.yunlin.com.tw.Api

import moto.yunlin.com.tw.GlobalVar.loginApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class LoginApi {

    val loginClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun login (memberId:String,password:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("memberId", memberId)
            .add("password", password)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(loginApiUrl)
            .build()

        val call: Call = loginClient.newCall(request)

        call.enqueue(callback)

    }
}