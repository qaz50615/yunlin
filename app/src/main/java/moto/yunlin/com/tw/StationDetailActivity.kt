package moto.yunlin.com.tw

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_station_detail.*
import moto.yunlin.com.tw.Adapter.StationDetailAdapter
import moto.yunlin.com.tw.Model.StationDetailModel
import java.io.Serializable
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import com.google.android.gms.maps.model.LatLng


class StationDetailActivity : AppCompatActivity() {

    companion object{
        const val AREA = "area"
        const val STATIONINFO = "stationInfo"
    }

    private var areaList = mutableListOf<StationDetailModel>()
    private var nAdapter: StationDetailAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_station_detail)

        initView()

    }

    private fun initView() {

        var stationList = intent.getSerializableExtra(STATIONINFO) as List<StationDetailModel>
        stationDetailTitle.text = intent.getStringExtra(AREA)

        nAdapter = StationDetailAdapter(this) {
            if (areaList.getOrNull(it)?.Latitude?.isEmpty() != true) {
                val intent = Intent(this, MapsActivity::class.java)
                intent.putExtra(MapsActivity.FROM, "station")
                intent.putExtra(MapsActivity.TITLE, areaList.getOrNull(it)?.SName ?: "")
                intent.putExtra(MapsActivity.STATIONINFO, areaList.getOrNull(it) as Serializable)
                startActivity(intent)
            }
//            } else {
//                var geo = Geocoder(this)
//                var latlng = geo.getFromLocationName(areaList.getOrNull(it)?.Address, 1)
//                if (latlng.size != 0 && latlng != null) {
//                    val latE6 = latlng[0].latitude
//                    val lngE6 = latlng[0].longitude
//                    val intent = Intent(this, MapsActivity::class.java)
//                    intent.putExtra(MapsActivity.FROM, "station")
//                    intent.putExtra(MapsActivity.TITLE, areaList.getOrNull(it)?.SName ?: "")
//                    intent.putExtra(MapsActivity.STATIONINFO,areaList.getOrNull(it) as Serializable)
//                    intent.putExtra(
//                        MapsActivity.STATIONINFO,
//                        areaList.getOrNull(it) as Serializable
//                    )
//                    intent.putExtra(MapsActivity.LAT,latE6)
//                    intent.putExtra(MapsActivity.LNG,lngE6)
//                    startActivity(intent)
//                }
//            }
        }

        stationDetailRv.layoutManager = LinearLayoutManager(this)
        stationDetailRv.adapter = nAdapter

        if (stationList.isNotEmpty()) {
            for (i in stationList.indices) {
                val area = stationList.getOrNull(i)?.Address?.substring(3, 6)
                if (area?.contains(intent.getStringExtra(AREA) ?: "") == true) {
                    areaList.add(stationList.getOrNull(i)!!)
                }
            }
            nAdapter?.updateData(areaList)
        }

        imageView9.setOnClickListener {
            finish()
        }
    }
}