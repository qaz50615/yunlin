package moto.yunlin.com.tw.Api

import okhttp3.*
import java.util.concurrent.TimeUnit

class GetPlaceLatlngApi {

    val getPlaceLatlngClient : OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun getPlaceLatlng (place:String,key:String,callback: Callback) {

        var url = "https://maps.googleapis.com/maps/api/geocode/json?language=zh-TW&"
        url += "address=$place"
        url += "&key=$key"

        val request = Request.Builder()
            .url(url)
            .get()
            .build()

        val call: Call = getPlaceLatlngClient.newCall(request)

        call.enqueue(callback)
    }
}