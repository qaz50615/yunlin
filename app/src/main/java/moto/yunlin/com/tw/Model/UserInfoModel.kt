package moto.yunlin.com.tw.Model

data class UserInfoModel(
    val code: String? = null,
    val errMsg: String? = null,
    val payload: UserInfoDataModel? = null
)