package moto.yunlin.com.tw.Model

data class StationDataModel(
    val fields: List<StationFieldModel>,
    val limit: Int,
    val offset: Int,
    val records: List<StationDetailModel>,
    val resource_id: String,
    val total: Int
)