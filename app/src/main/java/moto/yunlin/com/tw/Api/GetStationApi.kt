package moto.yunlin.com.tw.Api

import okhttp3.*
import java.util.concurrent.TimeUnit

class GetStationApi {

    val getStationClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun getStation (callback: Callback) {

//        val url = "http://opendata.epa.gov.tw/webapi/api/rest/datastore/355000000I-000011/?format=json&limit=$end&offset=$start&token=iMdT5+zVz0aKcg/oq+Giew"
        val url = "https://opendata.epa.gov.tw/webapi/api/rest/datastore/355000000I-000011?offset=2750&limit=200&sort=sno"
        val request = Request.Builder()
            .get()
            .url(url)
            .build()

        val call: Call = getStationClient.newCall(request)

        call.enqueue(callback)

    }
}