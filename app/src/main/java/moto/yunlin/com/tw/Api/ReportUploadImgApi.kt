package moto.yunlin.com.tw.Api

import moto.yunlin.com.tw.GlobalVar.uploadImgApiUrl
import okhttp3.*
import java.io.File
import java.util.concurrent.TimeUnit

class ReportUploadImgApi {

    val uploadImgClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun uploadImg (reportId:String,img: String,callback: Callback) {

        val body = FormBody.Builder()
            .add("reportId",reportId)
            .add("userfile",img)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(uploadImgApiUrl)
            .build()

        val call: Call = uploadImgClient.newCall(request)

        call.enqueue(callback)
    }
}