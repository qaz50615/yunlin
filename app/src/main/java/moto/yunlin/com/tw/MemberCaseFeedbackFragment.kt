package moto.yunlin.com.tw

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.KeyEvent.KEYCODE_BACK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_webview.*
import moto.yunlin.com.tw.GlobalVar.caseFeedbackUrl

class MemberCaseFeedbackFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.activity_webview, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        constraintLayout.visibility = View.GONE

        val webSetting = webView.settings
        // ----------------------------放大縮小
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = true
        // ----------------------------預設全顯
        webView.settings.useWideViewPort = true
        webView.settings.loadWithOverviewMode = true
        //
        webView.settings.setGeolocationEnabled(true)
        webView.settings.displayZoomControls = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.setAppCacheEnabled(true)
        webView.settings.databaseEnabled = true
        webView.settings.domStorageEnabled = true
        webSetting.javaScriptEnabled = true
        webSetting.javaScriptCanOpenWindowsAutomatically = true
        webSetting.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }
        }
        webView.loadUrl(caseFeedbackUrl+SP(activity).getid())

        imageView8.setOnClickListener {
            activity?.finish()
        }
    }
}