package moto.yunlin.com.tw

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.KeyEvent.KEYCODE_INSERT
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_moto_report.*
import moto.yunlin.com.tw.Api.GeocodingApi
import moto.yunlin.com.tw.Api.GetOSendReportApi
import moto.yunlin.com.tw.Api.GetPlaceLatlngApi
import moto.yunlin.com.tw.Api.ReportUploadImgApi
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import permissions.dispatcher.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.sqrt

@RuntimePermissions
class MotoReportActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener {

    companion object{
        const val CARNO = "carNo"
        const val REPORTID = "reportId"
    }

    private var FROMCAMERA = 1
    private var FROMALBUM = 0
    private var mMap: GoogleMap? = null
    private var flag = 0
    private var base64 = ""
    private var lat = 0.0
    private var lng = 0.0
    private var ads = ""
    private val locationManager: LocationManager by lazy {
        getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_moto_report)

        onNeedsPermissionWithPermissionCheck()
        initView()
//        setPermission()

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.uiSettings?.isMyLocationButtonEnabled = true
        mMap?.uiSettings?.isMapToolbarEnabled = true
        mMap?.uiSettings?.isZoomControlsEnabled = true
        if (checkSelfPermission()) {
            try {
                mMap?.isMyLocationEnabled = true
                mMap?.uiSettings?.isMapToolbarEnabled = true
                val lastKnownLocation = locationManager.getLastKnownLocation(findBestProvider())
                if (lastKnownLocation != null) {
                    lat = lastKnownLocation.latitude
                    lng = lastKnownLocation.longitude
                    mMap?.addMarker(
                        MarkerOptions()
                            .position(LatLng(lat, lng))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                    )
                    mMap?.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 16f)
                    )
                }
            } catch (e:Exception) {
                e.printStackTrace()
            }
        } else {
            setPermission()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                FROMCAMERA -> {
                    try {
                        if (data != null) {
                            val bitmap = data?.extras?.get("data") as Bitmap
                            base64 = convertToBase64(bitmap)
                            iv_pic.setImageBitmap(bitmap)
                        } else {
                            Toast.makeText(this,"照片擷取錯誤，請稍後再試！",Toast.LENGTH_LONG).show()
                        }
                    } catch (e:Exception) {
                        e.printStackTrace()
                    }
                }
                FROMALBUM -> {
                    try {
                        if (data != null) {
                            val uri = data?.data as Uri
                            val stream = contentResolver.openInputStream(uri)
                            var bitmap = BitmapFactory.decodeStream(stream)
//                        bitmap = compressionBitmap(bitmap, 150) as Bitmap
                            base64 = convertToBase64(bitmap)
                            iv_pic.setImageBitmap(bitmap)
                        } else {
                            Toast.makeText(this, "照片擷取錯誤，請稍後再試！", Toast.LENGTH_LONG).show()
                        }
                    } catch (e:Exception) {
                        e.printStackTrace()
                    }
                }
            }

        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun initView() {

        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment2) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val curDateTime = Date()
        val sdFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val date = sdFormat.format(curDateTime)
        tv1.text = intent?.getStringExtra(CARNO)
        tv2.text = date

        tv_pic.setOnClickListener {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, FROMALBUM)
        }

        tv_camera.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePictureIntent, FROMCAMERA)
        }

        tv_send.setOnClickListener {
            if (base64 != "") {
                if (ads.contains("雲林縣")) {
                    progressBar6.visibility = View.VISIBLE
                    GetOSendReportApi().getOrSendReport(intent?.getStringExtra(REPORTID)?:"",lat.toString(),lng.toString(),ads,Callback())
                } else {
                    Toast.makeText(this,"地址不在雲林縣內！",Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this,"請上傳照片！",Toast.LENGTH_SHORT).show()
            }
        }

        iv_back.setOnClickListener {
            finish()
        }

//        tv4.addTextChangedListener(object : TextWatcher{
//            override fun afterTextChanged(s: Editable?) {
//                progressBar6.visibility = View.VISIBLE
//                GetPlaceLatlngApi().getPlaceLatlng(s.toString(),getString(R.string.google_api_key),GetPlaceLatlngCallback())
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//            }
//
//        })

    }

    private fun setPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission()) {
                locationProvider()
            } else {
                val permissions =
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.CAMERA
                    )
                requestPermissions(permissions, 0)
                //跳視窗請求授權
            }
        }
    }

    private fun checkSelfPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    this,Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun locationProvider() {
        if (isGpsEnable()) {
            startGPS()
        } else {
            Toast.makeText(this, "請開啟高精確度定位模式", Toast.LENGTH_LONG).show()
        }
    }

    private fun compressionBitmap (bitmap:Bitmap, maxSize:Long):Bitmap? {
        return try {
            var width = bitmap.width
            var height = bitmap.height
            val maxLength = max(width,height)
            if (maxLength > maxSize) {
                val scale = sqrt(maxLength / maxSize.toDouble())
                width = (width / scale).toInt()
                height = (height / scale).toInt()
            }
            Bitmap.createScaledBitmap(bitmap,width,height,false)
        } catch (e:Throwable) {
            e.printStackTrace()
            null
        }
    }

    override fun onLocationChanged(location: Location?) {
        mMap?.clear()
        val mylocation = LatLng(location?.latitude?:0.0,location?.longitude?:0.0)
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 16f))
        mMap?.addMarker(MarkerOptions()
            .position(mylocation)
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        )
        val geocoder = Geocoder(this,Locale.TRADITIONAL_CHINESE)
        val addresses = geocoder.getFromLocation(mylocation.latitude,mylocation.longitude,1)
        ads = addresses[0].getAddressLine(0)
        Log.d("address",ads)
        tv4.text = ads
//        GeocodingApi().geocode(
//            lat,
//            lng,GetGeocodingCallback()
//        )
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }

    @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
    private fun findBestProvider(): String? {
        return locationManager.getBestProvider(setCriteria(), true) // 選擇精準度最高的提供者
    }

    private fun isGpsEnable(): Boolean {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    private fun startGPS() {
        val myLocation = locationManager.getLastKnownLocation(findBestProvider())
        if (myLocation != null) {
            lat = myLocation.latitude
            lng = myLocation.longitude
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat,lng), 16f))
            locationManager.requestLocationUpdates(findBestProvider(), 0, 20f, this)
            mMap?.addMarker(
                MarkerOptions()
                    .position(LatLng(lat,lng))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
            )
            val geocoder = Geocoder(this,Locale.TRADITIONAL_CHINESE)
            val addresses = geocoder.getFromLocation(lat,lng,1)
            ads = addresses[0].getAddressLine(0)
            Log.d("address",ads)
            tv4.text = ads
        }
    }

    @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
    private fun setCriteria(): Criteria {
        val criteria = Criteria()                    //資訊提供者選取標準
        criteria.accuracy = Criteria.ACCURACY_FINE  //设置定位精准度
        criteria.isAltitudeRequired = false          //是否要求海拔
        criteria.isBearingRequired = true           //是否要求方向
        criteria.isCostAllowed = false               //是否要求收费
        criteria.isSpeedRequired = true             //是否要求速度
        criteria.powerRequirement = Criteria.POWER_LOW      //设置相对省电
        criteria.bearingAccuracy = Criteria.ACCURACY_HIGH   //设置方向精确度
        criteria.speedAccuracy = Criteria.ACCURACY_HIGH     //设置速度精确度
        criteria.horizontalAccuracy = Criteria.ACCURACY_HIGH//设置水平方向精确度
        criteria.verticalAccuracy = Criteria.ACCURACY_HIGH  //设置垂直方向精确
        return criteria
    }

    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI19(context: Context, uri: Uri): String? {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
            } else if (isDownloadsDocument(uri)) {
                var cursor: Cursor? = null
                try {
                    cursor = context.contentResolver.query(
                        uri,
                        arrayOf(MediaStore.MediaColumns.DISPLAY_NAME),
                        null,
                        null,
                        null
                    )
                    cursor!!.moveToNext()
                    val fileName = cursor.getString(0)
                    val path =
                        Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                    if (!TextUtils.isEmpty(path)) {
                        return path
                    }
                } finally {
                    cursor?.close()
                }
                val id = DocumentsContract.getDocumentId(uri)
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:".toRegex(), "")
                }
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads"),
                    java.lang.Long.valueOf(id)
                )

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
        }
        return null
    }

    private fun convertToBase64 (bitmap:Bitmap):String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT)
    }

    private fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                              selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    @NeedsPermission(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )
    fun onNeedsPermission() {
        locationProvider()
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode,grantResults)
    }

    @OnShowRationale(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )
    fun onShowRationale(request: PermissionRequest) {
    }

    @OnPermissionDenied(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )
    fun onPermissionDenied() {
        android.app.AlertDialog.Builder(this)
            .setTitle("訊息提示")
            .setMessage("拒絕權限將導致部分功能無法正常執行,請問是否仍要拒絕授權？")
            .setPositiveButton("授權") { d, w ->
                val permissions =
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                requestPermissions(permissions, 0)
            }
            .setNegativeButton("確定") {d,w ->
                d.cancel()
            }
            .show()
    }

    @OnNeverAskAgain(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )
    fun onNeverAskAgain() {
    }

    inner class Callback: okhttp3.Callback{
        override fun onFailure(call: Call, e: IOException) {
            Log.d("sendReport-error", e.toString())
            runOnUiThread {
                progressBar6.visibility = View.INVISIBLE
                e.printStackTrace()
                AlertDialog.Builder(this@MotoReportActivity)
                    .setTitle("訊息提示")
                    .setMessage("系統或網路有問題，請稍後再試！")
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val jsonObject = JSONObject(response.body()?.string()?:"")
            val success = jsonObject.getString("code")
            if (success == "SUCCESS") {
                if (flag == 0) {
                    flag = 1
                    runOnUiThread {
                        ReportUploadImgApi().uploadImg(intent?.getStringExtra(REPORTID)?:"",base64,Callback())
                    }
                } else {
                    runOnUiThread {
                        finish()
                        Toast.makeText(this@MotoReportActivity,"檢舉送出成功！",Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                runOnUiThread {
                    progressBar6.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@MotoReportActivity)
                        .setTitle("訊息提示")
                        .setMessage("系統或網路有問題，請稍後再試！")
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }

    }

//    inner class GetGeocodingCallback: okhttp3.Callback{
//        override fun onFailure(call: Call, e: IOException) {
//            Log.d("getGeocoding-error", e.toString())
//            runOnUiThread {
//                progressBar6.visibility = View.INVISIBLE
//            }
//        }
//
//        override fun onResponse(call: Call, response: Response) {
//            try {
//                val jsonObject = JSONObject(response.body()?.string())
//                val resultArray = jsonObject.getJSONArray("results")
//                val status = jsonObject.getString("status")
//
//                if (status == "OK") {
////                    var addressComponents = JSONObject(resultArray[0].toString())
////                    var addressArray = addressComponents.getJSONArray("address_components")
////                    var longName = JSONObject(addressArray[4].toString())
//                    var formattedAddress = JSONObject(resultArray[1].toString())
//                    ads = formattedAddress.getString("formatted_address").toString()
//                    runOnUiThread {
//                        progressBar6.visibility = View.INVISIBLE
//                        tv4.text = ads
//                    }
//                } else
//                    Log.d("error Geocoding", status.toString())
//            } catch (e: JSONException) {
//                progressBar6.visibility = View.INVISIBLE
//                e.printStackTrace()
//            }
//        }
//    }

//    inner class GetPlaceLatlngCallback: okhttp3.Callback{
//        override fun onFailure(call: Call, e: IOException) {
//            Log.d("getLatLng-error", e.toString())
//            runOnUiThread {
//                progressBar6.visibility = View.INVISIBLE
//            }
//        }
//
//        override fun onResponse(call: Call, response: Response) {
//            try {
//                val jsonObject = JSONObject(response.toString())
//                val resultArray = jsonObject.getJSONArray("results")
//                val status = jsonObject.getString("status")
//
//                if (status == "OK") {
//                    var geometry = JSONObject(resultArray[0].toString())
//                    var geometryObject = geometry.getJSONObject("geometry")
//                    var location = geometryObject.getJSONObject("location")
//                    lat = location.getString("lat").toDouble()
//                    lng = location.getString("lng").toDouble()
//                    runOnUiThread {
//                        progressBar6.visibility = View.INVISIBLE
//                        mMap?.addMarker(MarkerOptions()
//                            .position(LatLng(lat,lng))
//                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
//                        )
//                    }
//                } else
//                    Log.d("error Geocoding", status.toString())
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//        }
//    }
}