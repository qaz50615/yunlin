package moto.yunlin.com.tw.Model

data class QueryCarNoModel(
    val code: String? = null,
    val errMsg: String? = null,
    val payload: CarInfoModel? = null
)