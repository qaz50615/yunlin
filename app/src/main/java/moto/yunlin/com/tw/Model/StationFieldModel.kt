package moto.yunlin.com.tw.Model

data class StationFieldModel(
    val id: String,
    val type: String
)