package moto.yunlin.com.tw.Api

import moto.yunlin.com.tw.GlobalVar.appLoginApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class AppLoginApi {

    val appLoginClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).build()

    fun appLogin (deviceId :String,callback: Callback) {

        val body = FormBody.Builder()
            .add("deviceId", deviceId )
            .add("deviceOSType", "android")
            .build()

        val request = Request.Builder()
            .post(body)
            .url(appLoginApiUrl)
            .build()

        val call: Call = appLoginClient.newCall(request)

        call.enqueue(callback)

    }
}