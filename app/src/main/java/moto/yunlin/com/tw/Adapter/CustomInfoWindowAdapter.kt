package moto.yunlin.com.tw.Adapter

import android.widget.TextView
import com.google.android.gms.maps.model.Marker
import android.app.Activity
import android.view.View
import com.google.android.gms.maps.GoogleMap
import moto.yunlin.com.tw.R


class CustomInfoWindowAdapter(private val context: Activity) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View {
        val view = context.layoutInflater.inflate(R.layout.custominfowindow, null)

        val tvTitle = view.findViewById(R.id.tv_title) as TextView
        val tvSubTitle = view.findViewById(R.id.tv_subtitle) as TextView

        tvTitle.text = marker.title
        tvSubTitle.text = marker.snippet

        return view
    }
}