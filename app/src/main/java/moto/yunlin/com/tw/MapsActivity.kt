package moto.yunlin.com.tw

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*
import moto.yunlin.com.tw.Adapter.CustomInfoWindowAdapter
import moto.yunlin.com.tw.Model.StationDetailModel
import permissions.dispatcher.*

@RuntimePermissions
class MapsActivity : AppCompatActivity(), LocationListener, OnMapReadyCallback {

    companion object {
        const val TITLE = "title"
        const val FROM = "from"
        const val STATIONINFO = "stationInfo"
//        const val LAT = "lat"
//        const val LNG = "lng"
    }

    private var mMap: GoogleMap? = null
    private var marker = MarkerOptions()
    private var lat = 0.0
    private var lng = 0.0
    private val locationManager: LocationManager by lazy {
        getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        onNeedsPermissionWithPermissionCheck()
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        initView()

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.uiSettings?.isMyLocationButtonEnabled = true
        mMap?.uiSettings?.isMapToolbarEnabled = true
        mMap?.uiSettings?.isZoomControlsEnabled = true
//        mMap?.setOnMapLoadedCallback {
//            when (intent.getStringExtra(FROM)) {
//                "nearBy" -> {
//                    val stationList = intent?.getSerializableExtra(STATIONINFO) as List<StationDetailModel>
//                    var last = LatLng(stationList[stationList.size-1].Latitude?.toDouble()?:0.0,stationList[stationList.size-1].Longitude?.toDouble()?:0.0)
//                    var first = LatLng(stationList[0].Latitude?.toDouble()?:0.0, stationList[0].Longitude?.toDouble()?:0.0)
//                    Log.d("stationLatLng","${stationList[0].Latitude}/${stationList[0].Longitude},${stationList[stationList.size-1].Latitude}/${stationList[stationList.size-1].Longitude}")
//                    mMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(LatLngBounds(first,last),5))
//                }
//            }
//        }
        if (checkSelfPermission()) {
            try {
                mMap?.isMyLocationEnabled = true
                when (intent.getStringExtra(FROM)) {
                    "nearBy" -> {
                        val stationList = intent?.getSerializableExtra(STATIONINFO) as List<StationDetailModel>
                        for (i in stationList.indices) {
                            val stationLat = stationList.getOrNull(i)?.Latitude
                            val stationLng = stationList.getOrNull(i)?.Longitude
                            if (stationLat?.isNotEmpty() == true && stationLng?.isNotEmpty() == true) {
                                marker.position(LatLng(stationLat.toDouble(), stationLng.toDouble()))
                                    .title(stationList.getOrNull(i)?.SName?:"")
                                    .snippet("${stationList.getOrNull(i)?.Address}\n${stationList.getOrNull(i)?.Tel}")
                                        .icon(
                                            BitmapDescriptorFactory.defaultMarker(
                                                BitmapDescriptorFactory.HUE_ORANGE
                                            )
                                        )
                                val adapter = CustomInfoWindowAdapter(this)
                                mMap?.setInfoWindowAdapter(adapter)
                                mMap?.addMarker(marker)
                                mMap?.setOnMarkerClickListener {
                                    it.showInfoWindow()
                                    val position = it.position
                                    lat = position.latitude
                                    lng = position.longitude
                                    return@setOnMarkerClickListener true
                                }
                            }
                        }
                        val myLocation = locationManager.getLastKnownLocation(findBestProvider())
                        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(myLocation.latitude,myLocation.longitude), 16f))
                    }
                    "station" -> {
                        val station = intent?.getSerializableExtra(STATIONINFO) as StationDetailModel
                        lat = station.Latitude?.toDouble() ?: 0.0
                        lng = station.Longitude?.toDouble() ?: 0.0
                        Log.d("station-location","$lat/$lng")
                        var marker = MarkerOptions()
                                .position(LatLng(lat,lng))
                                .title(station.SName?:"")
                                .snippet("${station.Address}\n${station.Tel}")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        val adapter = CustomInfoWindowAdapter(this)
                        mMap?.setInfoWindowAdapter(adapter)
                        mMap?.addMarker(marker)?.showInfoWindow()
                        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat,lng),16f))
                    }
                }

            } catch (e:Exception) {
                e.printStackTrace()
            }
        } else {
            setPermission()
        }
    }

    override fun onLocationChanged(location: Location?) {
        updateLocation(location)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }

    private fun initView() {

        tv_title.text = intent.getStringExtra(TITLE)

        iv_back.setOnClickListener {
            finish()
        }

        tv_go.setOnClickListener {
            if (lat != 0.0) {
                val uri = Uri.parse("google.navigation:q=$lat,$lng")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            } else {
                Toast.makeText(this,"請選擇一個定檢站！",Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun setPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission()) {
                locationProvider()
                //權限沒問題則取定位系統
            } else {
                val permissions =
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                requestPermissions(permissions, 0)
                //跳視窗請求授權
            }
        }
    }

    private fun checkSelfPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun locationProvider() {
        if (isGpsEnable()) {
            startGPS()
        } else {
            Toast.makeText(this, "請開啟高精確度定位模式", Toast.LENGTH_LONG).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
    private fun findBestProvider(): String? {
        return locationManager.getBestProvider(setCriteria(), true) // 選擇精準度最高的提供者
    }

    private fun isGpsEnable(): Boolean {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    private fun startGPS() {
        if (findBestProvider() != null) {
            locationManager.requestLocationUpdates(findBestProvider(), 0, 20f, this)
        }
    }

    @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
    private fun setCriteria(): Criteria {
        val criteria = Criteria()                    //資訊提供者選取標準
        criteria.accuracy = Criteria.ACCURACY_FINE  //设置定位精准度
        criteria.isAltitudeRequired = false          //是否要求海拔
        criteria.isBearingRequired = true           //是否要求方向
        criteria.isCostAllowed = false               //是否要求收费
        criteria.isSpeedRequired = true             //是否要求速度
        criteria.powerRequirement = Criteria.POWER_LOW      //设置相对省电
        criteria.bearingAccuracy = Criteria.ACCURACY_HIGH   //设置方向精确度
        criteria.speedAccuracy = Criteria.ACCURACY_HIGH     //设置速度精确度
        criteria.horizontalAccuracy = Criteria.ACCURACY_HIGH//设置水平方向精确度
        criteria.verticalAccuracy = Criteria.ACCURACY_HIGH  //设置垂直方向精确
        return criteria
    }

    private fun updateLocation(location: Location?) {
        if (location != null) {
            val myLocation = LatLng(location.latitude, location.longitude)
            Log.i("location", "$myLocation")
//            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16f))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode,grantResults)
    }

    @NeedsPermission(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun onNeedsPermission() {
        locationProvider()
    }

    @OnShowRationale(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun onShowRationale(request: PermissionRequest) {
    }

    @OnPermissionDenied(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun onPermissionDenied() {
        android.app.AlertDialog.Builder(this)
            .setTitle("訊息提示")
            .setMessage("拒絕權限將導致APP無法正常執行,請問是否仍要拒絕授權？")
            .setPositiveButton("授權") { d, w ->
                val permissions =
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                requestPermissions(permissions, 0)
            }
            .setNegativeButton("確定") {d,w ->
                d.cancel()
            }
            .show()
    }

    @OnNeverAskAgain(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun onNeverAskAgain() {
    }

}