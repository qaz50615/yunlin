package moto.yunlin.com.tw

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stx.xhb.androidx.entity.BaseBannerInfo
import com.stx.xhb.androidx.entity.LocalImageInfo
import kotlinx.android.synthetic.main.activity_main.*
import moto.yunlin.com.tw.Api.AppLoginApi
import moto.yunlin.com.tw.GlobalVar.fcmToken
import moto.yunlin.com.tw.GlobalVar.inspectionRemindUrl
import moto.yunlin.com.tw.GlobalVar.rawInfoUrl
import moto.yunlin.com.tw.GlobalVar.setUpUrl
import moto.yunlin.com.tw.yunlin.*
import moto.yunlin.com.tw.Adapter.ViewPagerAdapter
import moto.yunlin.com.tw.Api.GetStationApi
import moto.yunlin.com.tw.GlobalVar.areaList
import moto.yunlin.com.tw.GlobalVar.stationList
import moto.yunlin.com.tw.Model.InspectionStationModel
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import permissions.dispatcher.*
import java.io.IOException

@RuntimePermissions
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        onNeedsPermissionWithPermissionCheck()
        getToken()
        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

//        Glide.with(this).load(R.drawable.banner01).into(mainXBanner)

        val i = mutableListOf<BaseBannerInfo>()
        i.add(LocalImageInfo(R.mipmap.banner01))
        i.add(LocalImageInfo(R.mipmap.banner02))
        i.add(LocalImageInfo(R.mipmap.banner03))
        i.add(LocalImageInfo(R.mipmap.banner04))
        i.add(LocalImageInfo(R.mipmap.banner05))
        mainXBanner.setBannerData(i)

        mainXBanner.loadImage { banner, model, view, position ->
            val mModel = model as LocalImageInfo
            val mView = view as ImageView
            Glide.with(this).load(mModel.xBannerUrl).into(mView)
        }

        mainNoti.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }

        mainInspectionCheck.setOnClickListener {
            startActivity(Intent(this, InspectionActivity::class.java))
        }

        mainMotoQrcode.setOnClickListener {
            startActivity(Intent(this, MotoQrcodeActivity::class.java))
        }

        mainRawInfo.setOnClickListener {
            val intent = Intent()
            intent.putExtra(WebViewActivity.URL,rawInfoUrl)
            intent.putExtra(WebViewActivity.TITLE,"訊息法規")
            intent.setClass(this, WebViewActivity::class.java)
            startActivity(intent)
        }

        mainInspectionRemind.setOnClickListener {
            val intent = Intent()
            intent.putExtra(WebViewActivity.URL,"$inspectionRemindUrl$fcmToken")
            intent.putExtra(WebViewActivity.TITLE,"定檢提醒")
            intent.setClass(this, WebViewActivity::class.java)
            startActivity(intent)
        }

        mainInspectionStation.setOnClickListener {
            startActivity(Intent(this, InspectionStationActivity::class.java))
        }

        mainSetUp.setOnClickListener {
            val intent = Intent()
            intent.putExtra(WebViewActivity.URL,setUpUrl)
            intent.putExtra(WebViewActivity.TITLE,"安裝介紹")
            intent.setClass(this, WebViewActivity::class.java)
            startActivity(intent)
        }

        mainFeedback.setOnClickListener {
            startActivity(Intent(this, FeedbackActivity::class.java))
        }

        mainMember.setOnClickListener {
            startActivity(Intent(this, MemberActivity::class.java))
        }
    }

    private fun getToken() {

        FirebaseMessaging.getInstance().subscribeToTopic("global")
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            this@MainActivity,
            OnSuccessListener<InstanceIdResult> { instanceIdResult ->
                fcmToken = instanceIdResult.token
                progressBar7.visibility = View.VISIBLE
                AppLoginApi().appLogin(fcmToken,Callback())
                Log.i("FCMToken", fcmToken)
            })
        FirebaseMessaging.getInstance().isAutoInitEnabled = true

    }

    private fun checkSelfPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED )
    }

    private fun setPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkSelfPermission()) {
                val permissions =
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                requestPermissions(permissions, 0)
                //跳視窗請求授權
            }
        }
    }

//    private fun viewPagerSetting() {
//        val i = intArrayOf(
//            R.drawable.banner01,
//            R.drawable.banner02,
//            R.drawable.banner03,
//            R.drawable.banner04,
//            R.drawable.banner05
//        )
//        val al = ArrayList<ImageView>()
//        for (x in i.indices) {
//            val iv = ImageView(this)
//            iv.setBackgroundResource(i[x])
//            al.add(iv)
//            val v = View(this)
//            v.setBackgroundResource(R.drawable.point_normal)
//            //有多少張圖就放置幾個點
//            val layoutParams = LinearLayout.LayoutParams(15, 15)
//            layoutParams.leftMargin = 30
//            ll_container.addView(v, layoutParams)
//        }
//        mainVp.adapter = ViewPagerAdapter(al)
//        mainVp.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
//            override fun onPageScrollStateChanged(state: Int) {
//            }
//
//            override fun onPageScrolled(
//                position: Int,
//                positionOffset: Float,
//                positionOffsetPixels: Int
//            ) {
//            }
//
//            override fun onPageSelected(position: Int) {
//                val newPosition = position % al.size
//                ll_container.getChildAt(newPosition).setBackgroundResource(R.drawable.point_select)
//                ll_container.getChildAt(prePosition).setBackgroundResource(R.drawable.point_normal)
//                prePosition = newPosition
//            }
//
//        })
//        mainVp.currentItem = al.size * 1000  //這個是無線輪詢的關鍵
//        ll_container.getChildAt(0).setBackgroundResource(R.drawable.point_select)
//    }
//
//    private val nextImageTask = object : Runnable {
//        override fun run() {
//            runOnUiThread {
//                run {
//                    mainVp.currentItem = mainVp.currentItem +1
//                }
//            }
//        }
//    }

    @NeedsPermission(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    fun onNeedsPermission() {
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode,grantResults)
    }

    @OnShowRationale(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    fun onShowRationale(request: PermissionRequest) {
    }

    @OnPermissionDenied(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    fun onPermissionDenied() {
        android.app.AlertDialog.Builder(this)
            .setTitle("訊息提示")
            .setMessage("拒絕權限將導致部分功能無法正常執行,請問是否仍要拒絕？")
            .setPositiveButton("授權") { d, w ->
                val permissions =
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                requestPermissions(permissions, 0)
            }
            .setNegativeButton("確定") {d,w ->
                d.cancel()
            }
            .show()
    }

    @OnNeverAskAgain(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    fun onNeverAskAgain() {
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar7.visibility = View.INVISIBLE
                e.printStackTrace()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            Log.d("appLogin",response.body()?.string())
            runOnUiThread {
                progressBar7.visibility = View.INVISIBLE
            }
        }
    }
}
