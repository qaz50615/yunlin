package moto.yunlin.com.tw.Model

data class CarValidInfoModel(
    val code: Int,
    val msg: String,
    val result: String
)