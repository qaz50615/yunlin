package moto.yunlin.com.tw

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_member_info.*

class MemberInfoFragment : Fragment() {

    private var sp: SP? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_member_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        sp = SP(activity)
        textView5.text = sp?.getname()?:""

        tv_update.setOnClickListener {
            val intent = Intent(activity,SignUpActivity::class.java)
            intent.putExtra(SignUpActivity.PROPOSE,"editInfo")
            activity?.startActivity(Intent(activity,SignUpActivity::class.java))
        }

        tv_logout.setOnClickListener {
            sp?.clear()
            val fragmentManager = activity?.supportFragmentManager
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.fragment_container, MemberLoginFragment())?.commit()
        }
    }
}