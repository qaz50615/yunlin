package moto.yunlin.com.tw.Model

data class UserInfoDataModel(
    val Id: String? = null,
    val accountTitle: String? = null,
    val address: String? = null,
    val bankNo: String? = null,
    val bankTitle: String? = null,
    val createDate: String? = null,
    val deviceId: String? = null,
    val deviceOSType: String? = null,
    val email: String? = null,
    val memberId: String? = null,
    val memberStatus: String? = null,
    val memo: String? = null,
    val mobileNumber: String? = null,
    val name: String? = null,
    val phoneNumber: String? = null
)