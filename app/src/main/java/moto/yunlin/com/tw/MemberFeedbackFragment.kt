package moto.yunlin.com.tw

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragement_member_feedback.*
import moto.yunlin.com.tw.Api.QueryCarNoApi
import moto.yunlin.com.tw.Model.QueryCarNoModel
import okhttp3.Call
import okhttp3.Response
import java.io.IOException

class MemberFeedbackFragment : Fragment(){

    private var check = false
//    private var context: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragement_member_feedback, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        val sp = SP(activity)

        checkBox.setOnClickListener {
            check = if (!check) {
                checkBox.setImageResource(R.drawable.input_radio_on)
                true
            } else {
                checkBox.setImageResource(R.drawable.input_radio_off)
                false
            }
        }

        memberFeedbackRule.setOnClickListener {
            AlertDialog.Builder(activity)
                .setTitle("『會員檢舉辦法』")
                .setMessage("本人同意雲林縣環境保護局，於雲林縣機車定檢服務及未定檢回報系統整合APP系統使用時，利用訊息通知提醒機車排氣定期檢驗及獎勵資訊發佈之特定目的範圍內，蒐集本人個人資料，亦得提供其所蒐集本人個人資料提供給予電信業者利用及電腦處理通知作業（本資料僅供APP管理使用）。")
                .setPositiveButton("確定") {d,w ->
                    d.cancel()
                }
                .show()
        }

        memberFeedbackDone.setOnClickListener {
            var carNo = memberFeedbackNo.text.toString()
            if (sp.getname().isNotEmpty()) {
                if (sp.getmemberStatus() != "1") {
                    if (carNo.isNotEmpty()) {
                        if (check) {
                            carNo = carNo.replace("-","")
                            progressBar2.visibility = View.VISIBLE
                            QueryCarNoApi().queryCarNo(carNo,sp.getid(),Callback())
                        } else {
                            Toast.makeText(activity, "請詳讀會員檢舉辦法！", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(activity, "車牌號碼不得空白！", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(activity, "會員審核尚未通過！", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(activity,"請先登入會員！",Toast.LENGTH_LONG).show()
            }
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            activity?.runOnUiThread {
                progressBar2.visibility = View.INVISIBLE
                e.printStackTrace()
                androidx.appcompat.app.AlertDialog.Builder(context!!)
                    .setTitle("訊息提示")
                    .setMessage("網路或系統有誤，請稍後再試！")
                    .setPositiveButton("確定") { d, w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            val listType = object : TypeToken<QueryCarNoModel>() {}.type
            val queryCarNoModel: QueryCarNoModel = Gson().fromJson(mResponse, listType)

            if (queryCarNoModel.code.equals("SUCCESS")) {
                if (queryCarNoModel.payload?.checkStatus.equals("-3")) {
                    activity?.runOnUiThread {
                        progressBar2.visibility = View.INVISIBLE
                        androidx.appcompat.app.AlertDialog.Builder(context!!)
                            .setTitle("訊息提示")
                            .setMessage("無此車號")
                            .setPositiveButton("確定") { d, w ->
                                d.cancel()
                            }
                            .show()
                    }
                } else {
                    activity?.runOnUiThread {
                        progressBar2.visibility = View.INVISIBLE
                        val intent = Intent(activity, QueryCarNoResultActivity::class.java)
                        intent.putExtra(QueryCarNoResultActivity.CARINFO, mResponse)
                        intent.putExtra(QueryCarNoResultActivity.FROM, "feedback")
                        startActivity(intent)
                    }
                }
            } else {
                activity?.runOnUiThread {
                    progressBar2.visibility = View.INVISIBLE
                    androidx.appcompat.app.AlertDialog.Builder(context!!)
                        .setTitle("訊息提示")
                        .setMessage("系統或資料有誤，請稍後再試！")
                        .setPositiveButton("確定") { d, w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}