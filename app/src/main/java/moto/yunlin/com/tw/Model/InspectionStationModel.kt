package moto.yunlin.com.tw.Model

data class InspectionStationModel(
    val result: StationDataModel,
    val success: Boolean
)