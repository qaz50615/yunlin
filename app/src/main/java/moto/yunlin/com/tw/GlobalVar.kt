package moto.yunlin.com.tw

import moto.yunlin.com.tw.Model.StationDetailModel

object GlobalVar {
    var fcmToken = ""
    var stationList = mutableListOf<StationDetailModel>()
    var areaList = mutableListOf<String>()
    var server = "http://210.69.241.91/ylepb/api/v1/"
    var inspectionRemindUrl = "http://210.69.241.91/ylepb/user/user-family.html?deviceId="
    var rawInfoUrl = "http://210.69.241.91/ylepb/user/user-announce.html"
    var setUpUrl = "http://210.69.241.91/ylepb/user/user-howto.html"
    var caseFeedbackUrl = "http://210.69.241.91/ylepb/user/user-myreport.html?memberId="
    var loginApiUrl = "${server}queryMemberPW"
    var signUpNeditInfoApiUrl = "${server}updateMember"
    var queryCarNoApiUrl = "${server}queryCarNo"
    var reportApiUrl = "${server}report"
    var uploadImgApiUrl = "${server}uploadReportImg"
    var feedbackInfoApiUrl = "${server}inform"
    var appLoginApiUrl = "${server}loginApp"
}